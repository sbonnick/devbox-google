FROM ubuntu:16.04
MAINTAINER Stewart Bonnick <stewart.bonnick@autoclavestudios.com>

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.version=$VERSION \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://bitbucket.org/sbonnick/devbox-google" \
      org.label-schema.license="Apache-2.0"

RUN buildDeps='curl ca-certificates libssl-dev build-essential cmake' \
    && prodDeps='git ssh python' \
    && apt-get update && apt-get install -y $buildDeps $prodDeps --no-install-recommends \
    && curl -sSL https://deb.nodesource.com/setup_6.x | bash - \
    && apt-get install -y nodejs --no-install-recommends \
    && export CLOUDSDK_CORE_DISABLE_PROMPTS=1 \
    && curl -sSL https://sdk.cloud.google.com | bash - \
    && npm install -g bower polymer-cli firebase-tools \
    && echo '{ "allow_root": true }' > /root/.bowerrc \
    && npm cache clean \
    && apt-get purge -y --auto-remove $buildDeps \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 8080
