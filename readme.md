[![Run Status](https://api.shippable.com/projects/57936e7c3be4f4faa56d9ad8/badge?branch=master)](https://app.shippable.com/projects/57936e7c3be4f4faa56d9ad8)
[![Bitbucket issues](https://img.shields.io/bitbucket/issues/sbonnick/devbox-google.svg?maxAge=2592000)](https://bitbucket.org/sbonnick/devbox-google/issues)
[![Docker Pulls](https://img.shields.io/docker/pulls/sbonnick/devbox-google.svg?maxAge=2592000)](https://hub.docker.com/r/sbonnick/devbox-google/)
[![Image Layers](https://images.microbadger.com/badges/image/sbonnick/devbox-google.svg)](https://microbadger.com/images/sbonnick/devbox-google)
[![Image Version](https://images.microbadger.com/badges/version/sbonnick/devbox-google.svg)](https://microbadger.com/images/sbonnick/devbox-google)

# Google Devbox
The purpose of this docker image is facilitate creating disposable development sandboxes. 
This particular image is ideal when developing products/tools against google technologies 
such as polymer, firebase or gcloud.

## Examples using devbox-google directly

### Basic usage
```
docker run -P -it sbonnick/devbox-google:latest
```

### Providing a workspace and ssh credentials
This example shows how you can pass in a shared workspace if you want to work in and 
outside of the container. This also shows how to use your hosts system ssh for git repos 
so you dont have to retype passwords all the time. Use with caution though as you are 
sharing your ssh key with a container.
```
docker run \
    --name devbox \
    -p 8080:8080 \
    -v /home/user/workspace:/workspace \
    -v /home/user/.ssh:/.ssh \
    -it \
    sbonnick/devbox-google:latest
```
## Tooling
Here are some helpful tips when using the image, and the included tools.
### Serving static sites through Polymer
```
cd workspace
polymer serve --hostname 0.0.0.0
```
### Initializing GCloud with account information
```
gcloud init
```